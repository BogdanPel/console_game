package com.epam.view;

import com.epam.model.Character;
import com.epam.model.Enemy;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Interface for view requirements
 */
public interface View {
  void showGreetings();

  void readChooseCharacter();

  void battle();

  boolean isGameOver();

  void showDungeonEntrance();

  void onCharacterWeaponChanged(String newWeaponName);

  void onCharacterReady(String characterClass);

  void onEnemyReady(String enemyName, String enemyDescription);

  void onRoundStart(Character character, Enemy enemy);

  void onGameOver();

  void noHealthPotsLeft();

  void onEnemyDefeated();

  void onWinGame();
}
