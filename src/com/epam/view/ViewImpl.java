package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import com.epam.model.Character;
import com.epam.model.Enemy;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class with view requirements implementation
 */
public class ViewImpl implements View {

  private final Controller controller = new ControllerImpl(this);
  private final InputParser inputParser = new InputParser();
  private boolean isGameOver = false;

  @Override
  public void showGreetings() {
    System.out.println("Greetings, Traveler. Choose your class:");

    System.out.println("1:Warrior");
    System.out.println("2:Sorcerer");
    System.out.println("3:Priest");
  }

  @Override
  public void readChooseCharacter() {
    Scanner scanner = new Scanner(System.in);
    try {
      int input = scanner.nextInt();
      if (input > 0 && input <= 3) {
        controller.createCharacter(inputParser.parseCharacterChooseInput(input));
      } else throw new InputMismatchException("Please, play fair");
    } catch (InputMismatchException ex) {
      System.out.println("Please, play fair");
      readChooseCharacter();
    }
  }

  @Override
  public void battle() {
    controller.newBattle();
  }

  @Override
  public boolean isGameOver() {
    return isGameOver;
  }

  @Override
  public void onCharacterWeaponChanged(String newWeaponName) {
    System.out.println("Your character is armed with " + newWeaponName + ".");
  }

  @Override
  public void onCharacterReady(String characterClass) {
    System.out.println("You chose " + characterClass + "!");
  }

  @Override
  public void onEnemyReady(String enemyName, String enemyDescription) {
    System.out.println("You being attacked by " + enemyName + " : " + enemyDescription);
  }

  @Override
  public void showDungeonEntrance() {
    System.out.println("You have entered deep and dark place... Be careful.");
  }

  @Override
  public void onRoundStart(Character character, Enemy enemy) {
    System.out.println(
        "====================================YOUR STATS==========================================");
    System.out.println(
        " You are a "
            + character.getCharacterClassName()
            + " armed with "
            + character.getCharacterWeaponName()
            + " and you have "
            + character.getHealingPotCount()
            + " healing potions left.");
    System.out.println(" Your " + character.getHealth().toString());
    System.out.println(
        "====================================ENEMY STATS==========================================");
    System.out.println(
        " Your enemy is a "
            + enemy.getName()
            + " his "
            + enemy.getDamage().toString()
            + " and his "
            + enemy.getHealth().toString());

    System.out.println(
        "Here is what you can do: \n 1:Attack\n 2:Use health potion (+50 to current hp)");

    Scanner scanner = new Scanner(System.in);
    try {
      int input = scanner.nextInt();
      if (input == 1) {
        controller.attack();
      } else if (input == 2) {
        controller.useHealingPotion();
      } else throw new InputMismatchException("Please, play fair");
    } catch (InputMismatchException ex) {
      System.out.println("Please, play fair");
      onRoundStart(character, enemy);
    }
  }

  @Override
  public void onGameOver() {
    System.out.println("Game over. Git gud.");
    isGameOver = true;
  }

  @Override
  public void noHealthPotsLeft() {
    System.out.println("Whoops,You have no healing pots left,please chose attack");
  }

  @Override
  public void onEnemyDefeated() {
    System.out.println("You have defeated the enemy!");
  }

  @Override
  public void onWinGame() {
    isGameOver = true;
    System.out.println("You finished off all evil in dungeon and won the game!");
  }
}
