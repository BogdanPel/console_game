package com.epam.view;

import com.epam.model.CharacterClass;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class for parsing input value to character class
 */
public class InputParser {
  public CharacterClass parseCharacterChooseInput(int input) {
    switch (input) {
      case 2:
        return CharacterClass.Sorcerer;
      case 3:
        return CharacterClass.Priest;
      default:
        return CharacterClass.Warrior;
    }
  }
}
