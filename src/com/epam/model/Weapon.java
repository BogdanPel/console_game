package com.epam.model;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class for weapon
 */
public class Weapon {

  private int weaponDamage;
  private String name;

  public Weapon(String name, int weaponDamage) {
    this.name = name;
    this.weaponDamage = weaponDamage;
  }

  public int getWeaponDamage() {
    return weaponDamage;
  }

  public String getName() {
    return name;
  }
}
