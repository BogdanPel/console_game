package com.epam.model;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class for creature
 */
public abstract class Creature {

  Damage baseDamage;
  private Health health;

  Creature(Health health, Damage damage) {
    this.health = health;
    this.baseDamage = damage;
  }

  public Health getHealth() {
    return health;
  }

  public void setHealth(Health health) {
    this.health = health;
  }

  public Damage getDamage() {
    return baseDamage;
  }
}
