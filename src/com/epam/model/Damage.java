package com.epam.model;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class for damage
 */
public class Damage {
  private final int damage;
  private final int damageDispersion;

  public Damage(int damage, int dispersion) {
    this.damage = damage;
    this.damageDispersion = dispersion;
  }

  public Damage(int damage) {
    this.damage = damage;
    this.damageDispersion = 1;
  }

  public int getDamageValue() {
    return damage;
  }

  public int getDamageDispersion() {
    return damageDispersion;
  }

  @Override
  public String toString() {
    return "damage is " + damage + " +- " + damageDispersion;
  }
}
