package com.epam.model;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class for character
 */
public class Character extends Creature {

  private CharacterClass characterClass;
  private Weapon weapon;
  private Damage weaponCombinedDamage;
  private int healingPotCount = 1;

  public Character(
      CharacterClass characterClass, Health health, Damage baseDamage, Weapon baseWeapon) {
    super(health, baseDamage);
    this.characterClass = characterClass;
    armCharacter(baseWeapon);
  }

  public void armCharacter(Weapon weapon) {
    this.weapon = weapon;
    weaponCombinedDamage =
        new Damage(
            baseDamage.getDamageValue() + weapon.getWeaponDamage(),
            baseDamage.getDamageDispersion());
  }

  public int getCharacterDamageInThisCase() {
    DamageCalculator damageCalculator = new DamageCalculator(weaponCombinedDamage);
    return damageCalculator.getFinalDamage();
  }

  public String getCharacterClassName() {
    return characterClass.name();
  }

  public String getCharacterWeaponName() {
    return weapon.getName();
  }

  public int getHealingPotCount() {
    return healingPotCount;
  }

  public void decreaseHealingPots() {
    healingPotCount--;
  }

  public void addHealingPots(int potsCount) {
    healingPotCount += potsCount;
  }
}
