package com.epam.model;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class for enemy
 */
public class Enemy extends Creature {

  private EnemyClass enemyClass;

  public Enemy(EnemyClass enemyClass, Health health, Damage damage) {
    super(health, damage);
    this.enemyClass = enemyClass;
  }

  public String getName() {
    return enemyClass.name();
  }

  @Override
  public Damage getDamage() {
    return super.getDamage();
  }

  public int getEnemyDamageInThisCase() {
    DamageCalculator damageCalculator = new DamageCalculator(baseDamage);
    return damageCalculator.getFinalDamage();
  }

  public String getDescription() {
    switch (enemyClass) {
      case Skeleton:
        return "Spooky scary Skeleton!";
      case Lich:
        return "Very evil and very undead";
      case Vampire:
        return "He`s not just a sucker";
    }
    return "Very angry opponent";
  }
}
