package com.epam.model;

import java.util.Random;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class to calculate damage
 */
public class DamageCalculator {
  private final Damage damage;
  private final Random random;

  public DamageCalculator(Damage damage) {
    this.damage = damage;
    this.random = new Random();
  }

  public int getFinalDamage() {
    int dispersionPivot = damage.getDamageDispersion() / 2;
    int dispersionInCurrentCase = random.nextInt(damage.getDamageDispersion());
    int dispersionValue = dispersionPivot - dispersionInCurrentCase;
    int finalDamage = this.damage.getDamageValue() - dispersionValue;
    if (finalDamage < 1) return 2;
    else return finalDamage;
  }
}
