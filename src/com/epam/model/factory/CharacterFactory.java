package com.epam.model.factory;

import com.epam.model.Character;
import com.epam.model.CharacterClass;
import com.epam.model.Damage;
import com.epam.model.Health;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Factory for creating character
 */
public class CharacterFactory {
  private static final int PRIEST_MAX_HEALTH = 80;
  private static final int SORCERER_MAX_HEALTH = 100;
  private static final int WARRIOR_MAX_HEALTH = 120;
  private static final int PRIEST_DAMAGE = 4;
  private static final int PRIEST_DAMAGE_DISPERSION = 3;
  private static final int SORCERER_DAMAGE = 1;
  private static final int SORCERER_DAMAGE_DISPERSION = 12;
  private static final int WARRIOR_DAMAGE = 5;
  private static final int WARRIOR_DAMAGE_DISPERSION = 2;

  private CharacterClass characterClass;
  private WeaponFactory weaponFactory;

  public CharacterFactory(CharacterClass characterClass) {
    this.characterClass = characterClass;
    weaponFactory = new WeaponFactory();
  }

  public Character createCharacter() {
    switch (characterClass) {
      case Sorcerer:
        return createSorcerer();
      case Priest:
        return createPriest();
      case Warrior:
      default:
        return createWarrior();
    }
  }

  private Character createPriest() {
    Health priestHealth = new Health(PRIEST_MAX_HEALTH, PRIEST_MAX_HEALTH);
    Damage priestDamage = new Damage(PRIEST_DAMAGE, PRIEST_DAMAGE_DISPERSION);

    return new Character(
        CharacterClass.Priest, priestHealth, priestDamage, weaponFactory.createBasePriestWeapon());
  }

  private Character createSorcerer() {

    Health sorcererHealth = new Health(SORCERER_MAX_HEALTH, SORCERER_MAX_HEALTH);
    Damage sorcererDamage = new Damage(SORCERER_DAMAGE, SORCERER_DAMAGE_DISPERSION);
    return new Character(
        CharacterClass.Sorcerer,
        sorcererHealth,
        sorcererDamage,
        weaponFactory.createBaseSorcererWeapon());
  }

  private Character createWarrior() {
    Health warriorHealth = new Health(WARRIOR_MAX_HEALTH, WARRIOR_MAX_HEALTH);
    Damage warriorDamage = new Damage(WARRIOR_DAMAGE, WARRIOR_DAMAGE_DISPERSION);
    return new Character(
        CharacterClass.Warrior,
        warriorHealth,
        warriorDamage,
        weaponFactory.createBaseWarriorWeapon());
  }
}
