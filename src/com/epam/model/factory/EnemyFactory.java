package com.epam.model.factory;

import com.epam.model.Damage;
import com.epam.model.Enemy;
import com.epam.model.EnemyClass;
import com.epam.model.Health;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Factory for creating enemy
 */
public class EnemyFactory {
  private static final int VAMPIRE_MAX_HEALTH = 90;
  private static final int LICH_MAX_HEALTH = 175;
  private static final int SKELETON_MAX_HEALTH = 35;
  private static final int VAMPIRE_DAMAGE = 13;
  private static final int VAMPIRE_DAMAGE_DISPERSION = 2;
  private static final int LICH_DAMAGE = 2;
  private static final int LICH_DAMAGE_DISPERSION = 40;
  private static final int SKELETON_DAMAGE = 6;
  private final int fightNumber;

  public EnemyFactory(int fightNumber) {
    this.fightNumber = fightNumber;
  }

  public Enemy createEnemy() {
    EnemyClass enemyClass = initEnemyClass();
    Health enemyHealth = initEnemyHealth();
    Damage enemyDamage = initEnemyDamage();
    return new Enemy(enemyClass, enemyHealth, enemyDamage);
  }

  private Damage initEnemyDamage() {
    switch (fightNumber) {
      case 2:
        return new Damage(VAMPIRE_DAMAGE, VAMPIRE_DAMAGE_DISPERSION);
      case 3:
        return new Damage(LICH_DAMAGE, LICH_DAMAGE_DISPERSION);
      case 1:
      default:
        return new Damage(SKELETON_DAMAGE);
    }
  }

  private Health initEnemyHealth() {
    switch (fightNumber) {
      case 2:
        return new Health(VAMPIRE_MAX_HEALTH, VAMPIRE_MAX_HEALTH);
      case 3:
        return new Health(LICH_MAX_HEALTH, LICH_MAX_HEALTH);
      case 1:
      default:
        return new Health(SKELETON_MAX_HEALTH, SKELETON_MAX_HEALTH);
    }
  }

  private EnemyClass initEnemyClass() {
    switch (fightNumber) {
      case 2:
        return EnemyClass.Vampire;
      case 3:
        return EnemyClass.Lich;
      case 1:
      default:
        return EnemyClass.Skeleton;
    }
  }
}
