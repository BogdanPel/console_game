package com.epam.model.factory;

import com.epam.model.Weapon;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Factory for creating weapon
 */
public class WeaponFactory {
  private static final int WARRIOR_BASE_WEAPON_DAMAGE = 10;
  private static final int SORCERER_BASE_WEAPON_DAMAGE = 2;
  private static final int PRIEST_BASE_WEAPON_DAMAGE = 5;
  private static final int DROP_FROM_BOSS_WEAPON = 49;

  Weapon createBaseWarriorWeapon() {
    return new Weapon("Nobleman Longsword", WARRIOR_BASE_WEAPON_DAMAGE);
  }

  Weapon createBaseSorcererWeapon() {
    return new Weapon("Apprentice Staff", SORCERER_BASE_WEAPON_DAMAGE);
  }

  Weapon createBasePriestWeapon() {
    return new Weapon("Holy Censer", PRIEST_BASE_WEAPON_DAMAGE);
  }

  public Weapon getLoot() {
    return new Weapon("Universal Wonder Weapon", DROP_FROM_BOSS_WEAPON);
  }
}
