package com.epam.model;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07
 */
public class Health {
  /* current health field */
  private final int currentHealthPoint;
  /* max health field */
  private final int maxHealthPoint;

  /**
   * Constructor specifying current and maximum health to create.
   *
   * @param currentHealthPoint current health param
   * @param maxHealthPoint max health param
   */
  public Health(int currentHealthPoint, int maxHealthPoint) {
    if (currentHealthPoint > maxHealthPoint) {
      currentHealthPoint = maxHealthPoint;
    }
    this.currentHealthPoint = currentHealthPoint;
    this.maxHealthPoint = maxHealthPoint;
  }

  /**
   * This method is used for parsing to string.
   *
   * @return String with needed parameters
   */
  @Override
  public String toString() {
    return "Health is " + currentHealthPoint + " of " + maxHealthPoint;
  }

  /**
   * This method is used for getting current health.
   *
   * @return Return current health value
   */
  public int getCurrentHealthPoint() {
    return currentHealthPoint;
  }

  /**
   * This method is used for getting max value of health.
   *
   * @return Return max health value
   */
  public int getMaxHealthPoint() {
    return maxHealthPoint;
  }

  /**
   * This method checks if current health is more than 0.
   *
   * @return Return true if current health is greater than 0 , false otherwise
   */
  public boolean isAlive() {
    return currentHealthPoint > 0;
  }
}
