package com.epam.model;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Enum to define character class
 */
public enum CharacterClass {
  Warrior,
  Sorcerer,
  Priest
}
