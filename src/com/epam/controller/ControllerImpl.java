package com.epam.controller;

import com.epam.model.Character;
import com.epam.model.CharacterClass;
import com.epam.model.Enemy;
import com.epam.model.Health;
import com.epam.model.factory.CharacterFactory;
import com.epam.model.factory.EnemyFactory;
import com.epam.model.factory.WeaponFactory;
import com.epam.view.View;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Class with controller requirements implementation
 */
public class ControllerImpl implements Controller {

  private View view;
  private Character character;
  private Enemy currentEnemy;
  private int fightNumber = 0;

  public ControllerImpl(View view) {
    this.view = view;
  }

  @Override
  public void createCharacter(CharacterClass characterClass) {
    CharacterFactory factory = new CharacterFactory(characterClass);
    character = factory.createCharacter();
    view.onCharacterReady(character.getCharacterClassName());
    view.onCharacterWeaponChanged(character.getCharacterWeaponName());
  }

  @Override
  public void newBattle() {
    fightNumber++;
    if (fightNumber == 4) {
      view.onWinGame();
      return;
    }
    EnemyFactory enemyFactory = new EnemyFactory(fightNumber);
    currentEnemy = enemyFactory.createEnemy();
    view.onEnemyReady(currentEnemy.getName(), currentEnemy.getDescription());
    newRound();
  }

  @Override
  public void attack() {
    int characterDamage = character.getCharacterDamageInThisCase();
    int enemyDamage = currentEnemy.getEnemyDamageInThisCase();
    Health newCharacterHealth =
        new Health(
            character.getHealth().getCurrentHealthPoint() - enemyDamage,
            character.getHealth().getMaxHealthPoint());
    Health newEnemyHealth =
        new Health(
            currentEnemy.getHealth().getCurrentHealthPoint() - characterDamage,
            currentEnemy.getHealth().getMaxHealthPoint());
    character.setHealth(newCharacterHealth);
    currentEnemy.setHealth(newEnemyHealth);
    checkAliveness();
  }

  private void checkAliveness() {
    if (character.getHealth().isAlive()) {
      if (currentEnemy.getHealth().isAlive()) {
        newRound();
      } else {
        WeaponFactory factory = new WeaponFactory();
        character.armCharacter(factory.getLoot());
        character.addHealingPots(2);
        view.onEnemyDefeated();
        view.onCharacterWeaponChanged(factory.getLoot().getName());
      }
    } else {
      view.onGameOver();
    }
  }

  @Override
  public void useHealingPotion() {
    if (character.getHealingPotCount() != 0) {
      character.decreaseHealingPots();
      int enemyDamage = currentEnemy.getEnemyDamageInThisCase();
      Health newCharacterHealth =
          new Health(
              character.getHealth().getCurrentHealthPoint() - enemyDamage + 50,
              character.getHealth().getMaxHealthPoint());
      character.setHealth(newCharacterHealth);
      checkAliveness();
    } else {
      view.noHealthPotsLeft();
      newRound();
    }
  }

  private void newRound() {
    view.onRoundStart(character, currentEnemy);
  }
}
