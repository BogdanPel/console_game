package com.epam.controller;

import com.epam.model.CharacterClass;
/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07 Interface for view requirements
 */
public interface Controller {
  void createCharacter(CharacterClass characterClass);

  void newBattle();

  void attack();

  void useHealingPotion();
}
