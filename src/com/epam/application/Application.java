package com.epam.application;

import com.epam.view.View;
import com.epam.view.ViewImpl;

/**
 * @author Bogdan Pelikan pelikan.bogdan96@gmail.com
 * @version 0.0.1
 * @since 2019-09-07
 */
public class Application {

  public static void main(String[] args) {
    runApp();
  }

  private static void runApp() {
    View view = new ViewImpl();
    view.showGreetings();
    view.readChooseCharacter();
    view.showDungeonEntrance();
    while (true) {
      view.battle();
      if (view.isGameOver()) {
        return;
      }
    }
  }
}
